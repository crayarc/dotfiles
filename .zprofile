#
# ~/.bash_profile
#

[[ -f ~/.zshrc  ]] && . ~/.zshrc

# auto launch x on tty0

if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
	exec startx
fi

